package com.company.homework6;

import java.util.*;

public class MySet<T> implements Set<T> {
    private static final int DEFAULT_CAPACITY = 16;
    private final Node[] arr;
    private int size;

    private static class Node<T> {
        T value;
        Node<T> next;
    }

    public MySet() {
        this(DEFAULT_CAPACITY);
    }

    public MySet(int capacity) {
        this.arr = new Node[capacity];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    private int calcIndex(Object o) {
        return o.hashCode() % arr.length;
    }

    @Override
    public boolean contains(Object o) {
        int i = calcIndex(o);

        if (arr[i] == null) return false;

        for (Node<T> cur = arr[i]; cur != null; cur = cur.next) {
            if (cur.value.equals(o)) return true;
        }

        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private int i;
            private Node<T> cur;
            private int count;

            @Override
            public boolean hasNext() {
                return count != size;
            }

            @Override
            public T next() {
                while (cur == null && i < arr.length) {
                    cur = arr[i++];
                }

                T value = cur.value;
                cur = cur.next;
                count++;
                return value;
            }
        };
    }

    @Override
    public Object[] toArray() {
        return toArray(new Object[0]);
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        a = Arrays.copyOf(a, size);
        int i = 0;
        for (T t : this) {
            a[i++] = (T1) t;
        }
        return a;
    }

    @Override
    public boolean add(T t) {
        int i = calcIndex(t);

        if (arr[i] == null) {
            arr[i] = new Node<T>();
            arr[i].value = t;
            size++;
            return true;
        }

        for (Node<T> cur = arr[i]; cur != null; cur = cur.next) {
            if (cur.value.equals(t)) return false;
            else if (cur.next == null) {
                cur.next = new Node<T>();
                cur.next.value = t;
                size++;
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean remove(Object o) {
        int index = calcIndex(o);
        Node<T> head = arr[index];
        if (head == null) return false;
        if (Objects.equals(head.value, o)) {
            arr[index] = head.next;
            size--;
            return true;
        }
        for (Node<T> cur = head; cur != null; cur = cur.next) {
            if (Objects.equals(cur.next.value, o)) {
                cur.next = cur.next.next;
                size--;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(0)) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        c.forEach(this::add);
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        Object[] objects = new Object[0];
        for (T t : this) {
            boolean delete = true;
            for (var t1 : c) {
                if (Objects.equals(t, t1)) delete = false;
            }
            if (delete) {
                objects = Arrays.copyOf(objects, objects.length + 1);
                objects[objects.length - 1] = t;
            }
        }
        for (int i = 0; i < objects.length; i++) {
            remove(objects[i]);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        c.forEach(this::remove);
        return true;
    }

    @Override
    public void clear() {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = null;
        }
        size = 0;
    }
}
