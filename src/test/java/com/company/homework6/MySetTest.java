package com.company.homework6;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class MySetTest {
    @Test
    @DisplayName("Итератор")
    public void testIterator() {
        MySet<Integer> mySet = new MySet<>();
        mySet.add(0);
        mySet.add(16);
        mySet.add(16);
        mySet.add(1);
        mySet.add(2);
        mySet.add(3);
        mySet.add(3);
        mySet.add(15);
        assertEquals(6,mySet.size());
        Integer[] arr = new Integer[mySet.size()];
        int i = 0;
        for (Integer integer : mySet) {
            arr[i++]=integer;
        }
        assertArrayEquals(new Integer[]{0,16,1,2,3,15},arr);
    }

    @Test
    @DisplayName("В масив")
    public void testToArray() {
        MySet<Integer> mySet = new MySet<>();
        mySet.add(0);
        mySet.add(16);
        mySet.add(1);
        mySet.add(2);
        mySet.add(3);
        mySet.add(15);
        assertEquals(6,mySet.size());

        Integer[] arr = mySet.toArray(new Integer[0]);
        assertArrayEquals(new Integer[]{0,16,1,2,3,15},arr);

        Object[] objects = mySet.toArray();
        assertArrayEquals(new Object[]{0,16,1,2,3,15},objects);
    }

    @Test
    @DisplayName("Добавление и удаление")
    public void testAddAndRemove() {
        MySet<Integer> mySet = new MySet<>();
        mySet.add(0);
        mySet.add(16);
        mySet.add(16);
        mySet.add(1);
        mySet.add(2);
        mySet.add(3);
        mySet.add(3);
        mySet.add(15);
        assertEquals(6,mySet.size());

        Integer[] arr = mySet.toArray(new Integer[0]);
        assertArrayEquals(new Integer[]{0,16,1,2,3,15},arr);

        mySet.remove(16);
        mySet.remove(3);
        mySet.remove(0);
        assertEquals(3,mySet.size());

        arr = mySet.toArray(new Integer[0]);
        assertArrayEquals(new Integer[]{1,2,15},arr);
    }

    @Test
    @DisplayName("Очистка")
    public void testClear() {
        MySet<Integer> mySet = new MySet<>();
        mySet.add(0);
        mySet.add(16);
        mySet.add(1);
        mySet.add(2);
        mySet.add(3);
        mySet.add(15);
        assertEquals(6,mySet.size());

        Integer[] arr = mySet.toArray(new Integer[0]);
        assertArrayEquals(new Integer[]{0,16,1,2,3,15},arr);

        mySet.clear();

        arr = mySet.toArray(new Integer[0]);
        assertArrayEquals(new Integer[]{},arr);
    }

    @Test
    @DisplayName("Принадлежность")
    public void testContains() {
        MySet<Integer> mySet = new MySet<>();
        mySet.add(0);
        mySet.add(16);
        mySet.add(1);
        mySet.add(2);
        mySet.add(3);
        mySet.add(15);
        assertEquals(6,mySet.size());

        assertTrue(mySet.contains(0));
        assertTrue(mySet.contains(3));
        assertTrue(mySet.contains(15));
        assertFalse(mySet.contains(20));
        assertFalse(mySet.contains(5));
        assertFalse(mySet.contains(10));
    }

    @Test
    @DisplayName("*All")
    public void testAll() {
        MySet<Integer> mySet = new MySet<>();
        mySet.add(0);
        mySet.add(16);
        mySet.add(1);
        mySet.add(2);
        mySet.add(3);
        mySet.add(15);
        assertEquals(6,mySet.size());

        MySet<Integer> mySet1 = new MySet<>();
        mySet1.add(5);
        mySet1.add(6);
        mySet1.add(7);
        assertEquals(3,mySet1.size());

        mySet.addAll(mySet1);
        assertArrayEquals(new Object[]{0,16,1,2,3,5,6,7,15},mySet.toArray());

        assertTrue(mySet.containsAll(mySet1));

        mySet.removeAll(mySet1);
        assertArrayEquals(new Object[]{0,16,1,2,3,15},mySet.toArray());

        mySet1.clear();
        mySet1.add(1);
        mySet1.add(15);
        mySet1.add(3);

        mySet.retainAll(mySet1);
        assertArrayEquals(new Object[]{1,3,15},mySet.toArray());
    }
}